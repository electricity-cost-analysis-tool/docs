# Tariff Data Structure
This document describes with natural language, a format for storing a complete specification of an electricity tariff. This specification is based on analysis and design in [tariff-model.md](tariff-model.md). This specification is encoding agnostics to an extent, and a bit ambiguous. We mean JSON, Python, UTF8 ...

# Specification

## Units
In this version of the format, units shall not be specified explicitly in the data, but the following is assumed. Unless otherwise specified:

  + The unit of time is 30 minute intervals.
  + The length of time is a year.
  + The unit of electricity is the KWH.
  + The unit of capacity is KVA.
  + The unit of money is AUD.

## Structure
The data format is as follows. Note this specification does not preclude other fields from existing. Note [] indicates optional, <> a list, {} a choice, X|Y means X or Y.

        tariff ::-
          name ::- string
          [description ::- string]
          [outright_charge ::- <outright_charge>] // These are not temporal, and paid once - outright.
          [temporal_charge ::- <temporal_charge>] // Note "temporal" means a money per unit time charge.
          [volume_charge ::- <volume_charge>]
          [demand_charge ::- <demand_charge>]

        outright_charge ::-
          name ::- string
          [description ::- string]
          charge ::- number

        temporal_charge|volume_charge ::-
          name ::- string
          [description ::- string]
          charge ::- number|partition

        demand_charge ::-
          name ::- string
          [description ::- string]
          period ::- {'day', 'week', 'month', 'quarter', 'year'}
          minimum_charge ::- number
          charge ::- number|partition

        partition ::- partition is a special hierarchical data structure representing a series of interval values over time. See HBin.

**Other fields**
On the tariff may include, but be limited to:

        retailer: string
        type: string

This specification does not prohibit *any* additional fields anywhere.

**Partition vs Number**
For any number there is an equivalent partition. For example, given the assertion above about implicit units of time, the number 0.567 is equivalent to the partition:

  { value: 0.567, width: 364*48, label: 'year' }

The encoding should support representing a partition via a single number where possible.

**Names**
The `name` field should be unique amongst siblings.

## Validation
Currently not validator exists for any known encoding. IT is intended a Python and or JSON Schema specification be developed.

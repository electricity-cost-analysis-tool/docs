# Constraints & Requirements

  - Tariff cover must cover an entire calendar year.
  - Volume charge components of the tariff specify unambigously a charge for any and every interval of the year.
  - Human Readable.
  - Support presentation information.
  - Human Editable: Volume charge needs to be specified in a compact convenient human editable format.
  - Unfold: Volume charge components of the tariff can be unfolded into an array of values.
  - Foldup: A flat array can be folded up to a structure like the given volume charge with values corresponding to sums or averages.

# Volume Charge
Calculating volume charge is the non trivial part of this.

## Calculating Costs
Let T = [ts, te] be the period of time we want to calculate volume cost over. The cost for any given t in T is merely: V(t)*U(t). For any period it's merely the sum. Note the values `te` through `ts` can be represented by integers in range [0, <number-of-intervals-in-a-year>]. The question is how can this be done efficiently. Let U represent the array of readings. U is naturally an array. It will be provided as an array or at least can efficiently be converted to any array. Let V be the volume charge data structure. V is not an array.

There is two obvious approaches to calculating volume charge sums; U primary or V primary:

**V primary:**

    for p in V:
      s[p.name] = (p.charge*U.get_bits_matching(p)).sum()

*Arguments*:

  + In this case V is not required to be converted to an array or support indexed lookup (V(t)).
  + The result fits the shape of the applied charge.
  - The result may need to be reshaped anyway.
  - We may want to flatten the result for further analysis anyway.

**U primary:**

    for t in T:
      s[t] = U(t)*V(t)

*Arguments*

  + U is already an array and V only changes infrequently so can be converted to an array once and stashed.
  + Array multiplication is very efficient and highly parralelizable.
  - The result need to be reshaped to match the shape of the charge, so you end up having to run a similar logic to the above V primary loop, to "bin" the array values.

## Querying for Interval Sets
In the "V primary" scenario above the volume charge will be stored as a bag of ~queries for matching intervals or interval ranges. An interval is not just a value and an index. It's index is it's primary key, but it has other attributes: A interval is essentially a datetime range. It can be specified by broken down time components.  In UNIX for example:

    struct tm {
      int tm_sec;         /* seconds */
      int tm_min;         /* minutes */
      int tm_hour;        /* hours */
      int tm_mday;        /* day of the month */
      int tm_mon;         /* month */
      int tm_year;        /* year */
      int tm_wday;        /* day of the week */
      int tm_yday;        /* day in the year */
      int tm_isdst;       /* daylight saving time */
    };


Example query for summer, weekend, peak:

    i.month in [12,0,1] and i.weekday in [0,6] and i.hour in range [15,21]

# HBin Retrospective
Good:

  * Guaranteed to be a disjoint partition of a space.
  * Somewhat simple, intuitive hierarchical representation.
  * The unfold foldup operations are both supported, fairly easy to implement. Although possibly not so fast.

Bad:

  * No support for fractional repeated values. I.e. the months / weeks issue.
  * A dependency on a specific length or interval length. For example, all HBins in v1 of tariff-tool had to hve length = 65*48.

# HBin Alternatives
Supporting the foldup / unfold ~inverse operations are the most exacting requirements. All requirements are important though. Some starting constraints:

  1. The volume charge must repesent a hierarchical complete partition of intervals.
  2. Let us constrain ourselves to the UNIX broken down time components above.
  3. Each level of the hierarchy represents a complete partition of the range of interval to which it applies.

## Alt 1
Constrain the hierarchy to no more than 3 levels deep.

    L1: Partition the year.
    L2: Partition the week.
    L3: Partition the day.

Any of these partitions can be missing. Each partition should only be based on one broken down date time field

    L1: Can be based on month, week, or day.
    L2: Must be based on dayofweek
    L3: Must be based on hours (or minutes??).

Example:

  {
    label: "Year"
    partition_query { month: [[11,12,1,2,3], [4,5,6,7,8,9,10]] }
    partition_substructure: [
      {
        partition_query { wday: [[1,2,3,4,5],[0,6]] }
        partition_substructure: [
        ]
      },
      {
        ...
      }
    ]
  }

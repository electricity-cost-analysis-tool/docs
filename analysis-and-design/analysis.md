# Overview
This is the primary analysis and design document for the `electricity-tariff-analysis-web-tool` project.

## Aim
Broadly, the aim of this project is to develop a web based electricity tariff comparison *and analysis* tool. The target users are residential and small to medium enterprise electricity consumers. However, we aim to make the tool sufficiently general that it will also be of use to large consumers. Large consumers are generally on "business" tariffs and sometimes can negotiate custom contracts with retailers if they are large enough.

## Conceptual Overview
It's easiest to explain the concept for this product by comparing to an existing tool `Victorian Energy Compare`:

### Victorian Energy Compare
There is an existing web based tool that is currently live and implements a *subset* of the functionality we want to implement (although in a different way; i.e. we don't simply want to create a replica) called [Victorian Energy Compare](https://compare.switchon.vic.gov.au/). The core function of this site is to allow a user to compare existing retail electricity offers (i.e. tariffs), given their usage profile (also see [this analysis](./existing-retailer-evaluation-tools.md) of existing tools to aid consumers).

Given a user has access to their meter data the `Victorian Energy Compare` tool is used as follows:

**Step 1: Input Details**

  1. User inputs:
    - current retailer
    - post code
    - current distributor
    - whether they are eligible for any concession
  2. User uploads smart meter data file, and enters who provided them with their smart meter file (from list of retailers).
  3. User accepts terms and conditions.
  4. User presses next button.
  5. System displays results. See Step 2.

**Step 2: Results Page**

  + Once the user as done `Step 1: Input Details` a results page is shown. The results page shows:
    - Basic summary of details that were entered.
    - The users average daily consumption.
    - A list of offers from retailers, from least to most expensive when "Discounted Price" is considered (by default).
  + There are a number of things the user can do to refine or change search results including:
    - Order offers by Non "Discounted Price".
    - Show only top result from each retailer.
    - Filter by retailer.
    - Filter by other criteria (see the site for details).

Please refer to [Victorian Energy Compare](https://compare.switchon.vic.gov.au/) for further details of how this tool works. You can use [this sample smart meter data file](/data/sample-ami-meter-data-origin-20160401_20170401.csv) for testing.

Note the Victorian Energy Compare tool also allows the user to fill out a questionnaire instead of uploading their meter data file. However, for the scope of this project **the user must provide a smart meter interval data file as input**. Building a regression model to predict usage and or costs based off a questionnaire is a completely different problem to the one this project aims to solve.

### Comparison of Proposal to Victorian Energy Compare
The proposed tool is the same in that:

  + Users upload a smart meter data file, and some minimal other data such as post code as a first per-requisite step.
  + The system stores a collection of retail tariffs from real Victorian retailers.
  + The tool allows the user to estimate their cost under a given tariff.
  + The tool allows the user to compare the costs between different tariffs, primarily by presenting the user with a list of estimates ordered lowest to highest.

Broadly speaking, the proposed tool differs in that it:

  + Displays more in depth details about a users usage profile.
  + Does a better job of helping the user understand the breakdown of costs under a given tariff.
  + Allows the user to tweak the parameters of an existing tariff and analyze results (this is called "sensitivity analysis").
  + Allows the user to construct a tariff from scratch and compare the cost under that tariff just like any existing tariff.
  + Allows the user to simulate different DER (Distributed Energy Resource), and scheduled load scenarios (out of scope for MVP).

# Requirements Analysis
The following sections establish the fundamental requirements for the tool to be developed. It's important to note these requirements are not complete and may change as new facts and experience is gathered.

Different formats may be used to capture different requirements in the following sections.

## Functional Requirements
This section lists an *initial draft* of the important use cases for the proposed tool MVP. The following list is likely *incomplete*. It is intended that the tool be developed using an agile iterative development life cycle. As such all requirements are subject to change.

#### Use Case: Upload User Profile Data & Info
*Steps:*

  1. The user enters their post code and possibly some other information.
  2. The user upload a compatible smart meter data file.
  3. The system goes to `View Summary Information about Usage`

#### Use Case: View Summary Information about Usage
*Pre-requisites:* `Upload User Profile Data & Info` <br/>
*Description:*

  + The system will display a bunch of summary numbers and info about a users profile:
    - average daily usage as a number.
    - daily usage as a graph.
    - yearly usage as a graph broken down by month or quarterly or both (intent: quarter).
    - average weekly usage as a graph for each month or quarter or both (intent: quarter).

#### Use Case: Load Tariff
*Pre-requisites:* `View Summary Information about Usage` <br/>
*Steps:*

  1. The user selects they want to load a particular tariff.
  2. The system presents a list of tariffs.
  3. The user search through and selects the tariff they want to load.
  4. The system presents the details of the tariff.
    - TOU rate are displayed graphically.
    - If the tariff has different seasonal rates or TOU rates
  5. The system presents an estimate of the user's costs under the tariffs
    - Over a year.
    - For each season of a year.
    - For the average week of a season.
    - For the average week-day of a week
    - For the average weekend-day of a week
    - For the average day of a week-day
    - For the average day of a weekend-day

*Notes:* How this works - or can work, visually is fairly complicated, and needs some wire framing / web flow to describe properly.

#### Use Case: Compare Tariffs
*Pre-requisites:* `View Summary Information about Usage` <br/>
*Steps:*

  1. The user indicate they wish to compare estimated cost under existing Victorian tariffs.
  2. The system calculates the costs for available tariffs (intent: over a year), and displays the result to the used order lowest cost to highest cost.

#### Use Case: View Tariff Details
*Pre-requisites:* Compare Tariffs
*Description:* After a user has performed `Compare Tariffs` the user can "drill down" to see a break down of costs under this tariff. The drill down should be similar or the same (intent: same), as for the `Load Tariff` use case.

#### Use Case: Manage Tariff Database
*Description:* The database of tariffs for existing Victorian retailers needs to be populated some how, by an administrative user. Alternative approaches:

  - Injest a JSON file in some well defined format.
  - Use the UI described above in a R/W way.

### Data Formats & Processing
Basic requirements for handling smart meter data files are:

  + User electricity usage profiles shall be provided via the user's AMI smart meter.
  + The system may require that at least one year of data be present.
  + If more than one year of data is uploaded the system should trim the data to the latest whole year.
  + AMI smart meters record two independent streams of data; "export" and "import". Export is electricity export from the grid to the customer. Import is electricity imported to the grid from the customer, commonly called "feed in". In Victoria the minimum import tariff is [set by the Government](https://www.solarchoice.net.au/solar-rebates/solar-feed-in-rewards), but some retailers offer better rates. If the user does not import electricity, the data file will not contain an import stream. If they do, it should, and this will need to be used in cost estimation. That is import and export streams are not treated independently.
  + Some user's will have "dedicated loads" (sometimes called "controlled loads"). These are loads that are scheduled by the electricity retailer and metered separately. Electricity tariffs differ for scheduled loads. Scheduled loads should be treated independently. If a schedule load data stream is detected the user must decide whether which stream they want to analyze.
  + The system must be capable of detecting dedicated load data streams.

### Distributed Energy Resources DER and Scheduled Loads
Supporting DER is a stretch goal and not part of the MVP. However DER should be considered in the software design, to maximize extensibility. The following user stories outline the major use cases for supporting DER.

  + **Story 1:** As a user estimating my costs, I can see that under TOU electricity profile X my costs are Y, but what if I can shift 10% of my load to off peak times in summer?
  + **Story 2:** As a user estimating my costs, I can see that under profile X my costs are Y, but what if I added solar panels to my house of capacity Z?
  + **Story 3:** As a user estimating my costs, I can see that under profile X my costs are Y, but what if I added a battery with capacity parameters A,B,C ..?
  + **Story 4:** As a user estimating my costs, I can see that under profile X my costs are Y, but what 10% of my load is potentially schedulable

# Design
See [design.md](design.md).

# Tariff Model
A fairly general model of a tariff should be able to capture the following components:

  + A fixed charge (outright charges; for example setup/connection charge. Units of $).
  + A fixed daily charge ($/day).
  + A volume charge ($/kWh).
  + A capacity charge (based on peak usage. Also known as demand fee. Units ~ $/kVA/pa).
  + Feed in component ($/kWh feed back into the grid from on site generation).

The *fixed charge* and *fixed daily charge* components could be represented as single numbers. The *feed in* component might be best represented as a tariff itself (i.e. Tariff is a recursive data structure). The *capacity charge* has fairly simple sub structure. A generic model of the *volume charge* has a nested structure.

Note, there might be more than one of each of the above components, so the model should support a labeled list of each type, rather than just a single instance.

The following image shows the above components as a class diagram:

![tariff](/img/tariff-model-v1.png)

The structure of the capacity charge, and volume charge are discussed in the next sections.

## Capacity Charge
**A Case Study:** Citipower's capacity fee is calculate by running this algorithm every month:

  1. If this is the first ever billing month then set `max_demand` = 0.
  2. If `max_demand` has had the same value for 12 months, then reset `max_demand` to 0.
  3. After each month, let `max_demand` = max(`max_demand`, maximum demand observed in any interval reading in the month just past).
  4. The monthly capacity charge is `max_demand` * rate.

They call this a "rolling charge". Most capacity charges are the same as Citipower's (where present). One variation the tariff model should capture is the rollover period. For example the rollover period could be a day, week, month, or year. So essentially it suffices to represent a Capacity charge with three components:

  1. `charge` in units of $/KVA.
  2. `period` over which the rate applies in units of time (example 1 day, 1 week, 1 month).
  3. `rollover_window` length. This can be a multiplier of the period (example 12).

It seems common to present `charge` and `period` in the same number. For example "$80/KVA/day" indicates a charge of $80 and a period of one day. How they should be represented in implementation is left undefined.

Citipower's rolling charge scheme is quite unintuitive (perhaps intentionally). In more simple capacity charges, there will be no `rollover_window`. A `rollover_window` setting of 1 (or 0) could be used to indicate this.

**Example 1:**

    charge=$80/KVA; period=MONTH; rollover_window=12

**Example 2:**

    charge=$80/KVA; period=DAY; rollover_window=0

### Load Factor
Capacity charges may be based on kVA *not* kWh. kVA can be derived from kWh by multiplying the kWh reading by the `1 / load_factor`. The load factor may change with every reading and is a number between [0,1].

Residential meters currently don't include a `load_factor` recording, and any capacity charge will be based on kWh. Some business plan meters do record a `load_factor` stream. Normally the load factor will be close to 1, but not always.

Supposing the system use kWh readings to calculate capacity charges on kWh not KVA:

  - Consistent for residential readings.
  - The accuracy will depend on the `load_factor` of the individual premises for business users.

## Volume Charge
As an empirical observation, the simplest volume charge observed in the market today is a single rate for the entire year. The most complex volume charge we've found in the Australian residential market is structured like this:

    Summer
      Week-day
        on/shoulder/off
      Week-end
        on/shoulder/off
    Winter
      Week-day
        on/shoulder/off
      Week-end
        on/shoulder/off

In the future, we are likely to see more complex tariff structures than this. The most likely additional complexity in the volume charge will be:

  1. Adding Seasons to give *Summer, Autumn, Winter, Spring*.
  2. Adding extra components to the daily TOU components, or simply defining it a list of prices for every interval.

Also, there may be variation in the way a week or day is split up:

  1. Sometimes there is no distinction between weekday and weekend.
  2. A retailer might define 3 week parts or define weekend to include Friday.
  3. Exactly what periods of time "on" and "off" are can vary.

There are also inclining/declining block volume charges, that can be applied combination with TOU and seasonal change!

*The challenge is constructing a single, elegant model of the volume component, that can capture the simple fixed rate to the most complex, and everything in between.*

### Model for Volume Charge
Consider a user's usage data. For the scope of this project its a year's worth of interval meter readings. Assuming the interval is 30m, the usage data is just an array with *52\*7\*24\*2=17472* values (or depending on how you define a year *365\*24\*2=17520* values. Close enough either way, for an analysis of costs over a year). Regardless of the complexity of the volume charge, for each of the 17472 intervals, *t*, we can define a volume charge, *V(t)*. The cost at *t* is thus:

    Cost(t) = V(t)*U(t)

Where *U(t)* is the usage. And the cost over year or arbitrary period of time is the sum of *Cost(t)* all *t* over that year or arbitrary period of time.

To use this approach, we need to be able to defined *V(t)* for *every* interval of a year in order to accurately calculate costs.

Consider a fixed volume charge that doesn't change throughout the year, say *$0.23/kWh*. This is just a succinct way of saying the volume charge is the same for every interval of the year. We can find the volume charge at any time of the year because is the same value ...

More complex volume charges slice and dice up the year and define different prices for different parts of the year. But they tend to slice and dice in fairly regular ways. The challenge is, to represent such a tariff in such a way that for any point in the year (one of the 17472 intervals) we can easily query the tariff structure to get the price at that time. But we also have other requirements:

  - The tariff should be stored efficiently.
  - Use the charge to calculate a cost break down as well as total costs.
  - Inspect different components of the volume charge and display them to the user.
  - Inclining and declining block rates.

### *Proposal* for Tariff Data Structure
As a raw untyped JSON object the volume charge will be represented by a nested data structure, but with a fixed depth. Each level of the data structure represents a level of a *hierarchical partition* of a year. For our purposes the data structure will have at most 3 levels:

  - A Year is composed of 52 Weeks
  - A Week is composed of 7 Days
  - A Day is composed of 48 Intervals
  - An Interval is just a number

It slightly more complicated than this because each level can be split up into parts. Exactly how they are split can vary. So the proposed data structure is specifically designed to be able to handle arbitrary "splitting". However, commonly:

  - A Year is split into seasons ({Summer, Autumn, Winter, Spring}), or semesters ({Summer, Spring})
  - A Week is split into weekdays and weekend days.
  - A Day is split into on and off peak times.

The following two *typescript* interfaces could be used to construct the volume tariff:

    interface Partition {
        label: string;
        width: number;
        value: any;
    }

    interface PartitionGroup extends Partition {
      childs: Array<Partition>;
    }

#### Inclining/Declining Block Rate Charge
A non inclining/declining block rate charge for any given interval will just be a number. A block rate charge can be distinguished from this by storing an object or dictionary type where a number would otherwise be. An inclining block rate stored as a dictionary *might* look like:

    { 1.6: 0.2, 3.0: 0.22, 5.0: 0.24, 10.0: 0.3 }

#### On Units
Dealing with units can be a challenge. Where are units important:

  - Meter reading interval lengths and values.
  - Tariff charge interval lengths and values.
  - Calculated costs.

Rather than representing unit explicitly, to simplify things the strategy we will most likely use is to declare in a standard what the interpretation of values in a data structure should be.

#### Examples of Proposed Tariff Data Structure
In the following examples, instead of listing one deeply nested data structure, I've broken the data structure down into a number of variables.

**Example 1:** A single value tariff say $0.23/kWh for the entire year is represented like this:

    var day:Partition = {
      label: 'day',
      width: 48,
      value: 0.23
    }
    var week:Partition = {
      label: 'week',
      width: 7,
      value: day
    }
    var year:Partition = {
      label: 'year',
      width: 52,
      value: week
    }

Granted this is a fairly verbose way to represent a single number. So maybe the system should store such a tariff as a single number, detect this, and create the above data structure from it when needed.

**Example 2:** Winter/Summer with weekday/weekend distinction but a flat rate daily rate for all days:

    var winter_weekday_day:Partition = {
      label: 'day',
      width: 48,
      value: 0.23
    }
    var winter_weekend_day:Partition = {
      label: 'day',
      width: 48,
      value: 0.19
    }
    var summer_weekday_day:Partition = {
      label: 'day',
      width: 48,
      value: 0.25
    }
    var summer_weekend_day:Partition = {
      label: 'day',
      width: 48,
      value: 0.22
    }
    var winter_week:PartitionGroup = {
      label: 'week',
      width: 7,
      value: null,
      childs:[
        {
          label: 'weekdays',
          width: 5,
          value: winter_weekday_day
        },
        {
          label: 'weekend',
          width: 2,
          value: winter_weekend_day
        }
      ]
    }
    var summer_week:PartitionGroup = {
      label: 'week',
      width: 7,
      value: null,
      childs:[
        {
          label: 'weekdays',
          width: 5,
          value: summer_weekday_day
        },
        {
          label: 'weekend',
          width: 2,
          value: summer_weekend_day
        }
      ]
    }
    var complex_year:PartitionGroup = {
      label: 'year',
      width: 52,
      value: null,
      childs: [
        {
          label: 'winter',
          width: 26,
          value: winter_week
        },
        {
          label: 'summer',
          width: 26,
          value: summer_week
        }
      ]
    }

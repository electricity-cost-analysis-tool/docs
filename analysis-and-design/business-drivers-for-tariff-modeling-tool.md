# Business Drivers for Modeling Tariffs
[Clean Energy Council](http://www.cleanenergycouncil.org.au/) recently completed a report into opportunities for small to medium business enterprises (SMEs) to reduce their energy bills through energy efficiency [1][1]. While the opportunities and motivations vary from business to business, the key factors determining costs are:

  - The business's load profile and flexibility in that profile.
  - Available tariff structures.
  - Distributed energy resources (DER) technologies deployed at the business (batteries, PV, etc).

Regarding tariffs, the report states:

> *"At any given location in the NEM, an SME may have access to something in the order of 50 tariffs from around 10 retailers. And between locations, there will be further variation in tariffs available."* [1][1].

For a given tariff, the costs may also be heavily dependent on the business's specific load profile. The load profile is in turn effected by on site technology such as battery storage, and solar panels. These factors in combination make selecting the correct tariff structure a bewildering prospect to many SMEs.

While there may be in the order of 50 tariff structures available the to the SME, the same Clean Energy Council report states that almost all tariffs can be completely described by four components:

  1. A fixed fee ($/day).
  2. A volume fee ($/KWH).
  3. A capacity fee (based on peak usage. Also known as "demand" fee).
  4. Feed in fee ($/KWH feed back into the grid from on site generation).

Within each of these components there is also variation, but it tends to also have common structure. The *upshot* here is, given an accurate model of a tariff, and an accurate record of a customers consumption over some period, it should possible to accurately estimate how much consumer would pay for electricity. Further, given information on DER devices deployed or potentially deployed at the site, it should also be possible to estimate the effect of those DER devices on the costs.

Currently (AFAIK), freely available web based tools to support the SME in performing the above estimates, doing sensitivity analysis, and basic modeling are lacking (see this [report](/analysis-and-design/existing-retailer-evaluation-tools.md).

[1]: http://fpdi.cleanenergycouncil.org.au/reports/improving-electricity-use-in-small-business.html

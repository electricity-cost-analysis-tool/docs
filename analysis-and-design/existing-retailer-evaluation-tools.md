# Review of Existing Tariff Comparison and Analysis Tools
There are currently (May 2017) at least 24 electricity retailers operating in VIC. Despite this, market competition has not resulted in a significantly reduced prices for consumers (in fact, the opposite has occurred). Recently retailers have been accused of price gouging. But still, VIC consumers are reluctant to test the market and switch providers. The reasons for this may be because:

  + The cost involved in actually figuring out whether you better off with a different provider might not be worth the effort.
  + Due to complexity in tariff structures by retailers there is a high level of real or perceived uncertainty in predictions made.
  + Consumers may be unaware of online tools available to help them make an informed decision.

## What Online Tools *Are* Available?
[This AER website](https://www.aer.gov.au/consumers/switching-retailers) provides advice for consumers about finding a good retailer. Their site includes a rudimentary enumeration of online tools:

  - [Energy Made Easy](http://www.energymadeeasy.gov.au/) is the primary tool created by AER but is currently NSW only.
  - [Vic Energy Compare](https://compare.switchon.vic.gov.au/) is the VIC version of above.
  - "Commercial switching sites" are listed as an option. These site apparently automatically switch you to a better retailer for a commission. No examples are listed.

###  A Web Search for Tools
Top page in [web search](https://duckduckgo.com/?q=compare+electricity+victoria&t=ffab&ia=web) yields above sites and related govt sites plus:

  + https://www.comparethemarket.com.au
    Same concept as the govt sites. Wants you contact details including mobile before giving you results.
  + https://lumoenergy.com.au/home-energy/switch-to-lumo/victoria
    Retailer site.
  + http://www.canstarblue.com.au/energy/electricity/vic-providers/
    User star based rating. Good resource for an very rough test.
  + https://www.switchwise.com.au/
    Defunct.
  + http://energy.iselect.com.au/compareplans/
    Same concept as the govt sites.
  + https://compare.electricitywizard.com.au/about-you`
    Same concept as the govt sites.
  + https://www.goswitch.com.au/compare-electricity/
    A switching site: GoSwitch receives a commission from energy companies when customers switch through us. They have an arrangement with 4 retailers so you'll only ever get one of those four. Crock of shit.
  + http://youcompare.com.au/energy/quotes
    Same concept as the govt sites. Form is much simpler - only one page of about 6 inputs.

### Summary
There are really three classes of tool available right now:

  1. Form driven comparators.
  2. Star rating hubs.
  3. Commission based switching sites.

Comparators like [Vic Energy Compare](https://compare.switchon.vic.gov.au/) seem the most useful and powerful for consumers.

## Comparators
Form driven comparators seem the most prominent. There is two alternate work-flows, from two input format with these tools:

  1. Fill out a questionnaire to estimate usage.
  2. Upload usage data (at least one year worth in all cases).

The **forms** are laborious to fill out and the results are inaccurate. However, rhe smart meter roll out is complete in VIC, so users can now obtain and upload their usage data as the basis of analysis. Access to usage data is no longer a barrier. Even still there are issues with existing comparators:

  - They just throw back a bunch of suggestions at you. You can't easily tell *why* the suggestion was returned. I.e. there is now traceability.
  - No sensitivity analysis. For example: What happens if you could reduce your peak load 10% in peak period in summer?
  - Lack of trust in non govt comparators.
  - Most of the non govt forms require personal info - at least mobile and email.

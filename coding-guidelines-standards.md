# General Coding Guidelines
These apply in general.

  * Code in a consistent style.
  * Document all methods, classes, and functions.
  * Keep white space and commenting within method bodies to a minimum.
  * Follow the best practice of the system / language you are using.
  * Explain systems or stand alone repositories briefly in a README.md.
  * Follow a ~test driven development by developing unit tests as soon as you cut code.
  * Keep formal unit tests under `tests/`, unless the system your working with has other requirements.
  * If you want to keep manual test script about in repos keep them under `tests-inf/`, or possibly `script/`

# Python Coding Standard

  * We're using python 3.4 or above.
  * Observe the PEP8 coding standard. Use the `pep8` sniffer ~like `pep8 my.py --max-line-length=120`.
  * Organise your so test discovery works - https://docs.python.org/3.4/library/unittest.html#unittest-test-discovery.
  * Name files and dirs all lower case with no "_" or "-".
  * Try and document the dependencies your code has in manifests or if not README.md files.

## Python Logging & Debug

  * Use logging (https://docs.python.org/3.4/library/logging.html) instead of printing to stdout.
  * Create a logger like this in you modules `myLogger = logging.getLogger(__name__)`.
  * Dont log on `logger` directly - `logging.debug(...)` - unless in main of a script.

## Python Testing

  * Use the `unittest` module for unit testing.
  * Keep formal unit tests under `tests/`, informal tests in `tests-inf/` or `scripts/`
  * In general little reason not to have 100% coverage (barring possibly execption bodies). Note, overage reporting in python3: `python3-coverage run tests/all.py python3-coverage report`
  * E2E tests should compliment unit tests.
  * Manual checklists may also be useful in compliment E2E and unittests, for tests that are hard to automate - like ~UX.

# Other Programming Languages

  * Generally seek out the most common coding standard. Some languages have one main one (example PHP has PSR). If not pick one and document the choice.

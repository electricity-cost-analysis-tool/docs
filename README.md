# Electricity Tariff Analysis Web Tool
This repository holds documentation for a project that aims to develop software tools for the comparison and analysis of cost of electricity, under various tariffs, usage profiles and eventually mixes of onsite generation and storage.

# Important Documentation
The most important documentation for this project is as follows. Reading in order presented is recommended.

  - [Business Drivers For Tariff Modeling Tools](/analysis-and-design/business-drivers-for-tariff-modeling-tool.md)
  - [A Review of Existing Retail Tariff Analysis Tools](/analysis-and-design/existing-retailer-evaluation-tools.md)
  - [Intial Analysis](/analysis-and-design/analysis.md)
  - [Initial Design Notes](/analysis-and-design/design.md)

# Software Architecture
There are a number of related software packages that have been developed in this project. The following lists the core packages within scope. Packages are all maintained [here](https://gitlab.com/electricity-cost-analysis-tool/).

**AMI Data Parser** <br/> Preprocesses smart meter data files of various formats into a convenient and homongeneous form used in any further analysis. See [ami_data_parser](https://gitlab.com/electricity-cost-analysis-tool/ami_data_parser).

**Tariff Data Model** <br/> A generic schema that can describe a wide range of retail TOU tariffs. See [tariffs](https://gitlab.com/electricity-cost-analysis-tool/tariffs).

**Tariff Database** <br/> A database of tariffs from various retailers. See [tariffs](https://gitlab.com/electricity-cost-analysis-tool/tariffs).

**Analytics** <br/> The [web_api](https://gitlab.com/electricity-cost-analysis-tool/web_api) is a JSON ReST web service that takes AMI data file and returns various cost and usage breakdowns. The [cli tool](https://gitlab.com/electricity-cost-analysis-tool/cli) does the same thing from a UNIX command line. Both these tools use [hbin]([web_api](https://gitlab.com/electricity-cost-analysis-tool/hbin) to generate cost and usage breakdowns given a tariff and usage data.

**Web Front End** <br/> The [web_ui](https://gitlab.com/electricity-cost-analysis-tool/web_ui) user the web_api to display analytics result to the user in a nice HTML UI.

# Contributing

## Coding Standards & Guidelines
In general see [coding-guidelines-standards.md](coding-guidelines-standards.md). A given repository in this project may have more specific guidelines for contributing.

## KanBan
A single [kanban board](https://gitlab.com/electricity-cost-analysis-tool/docs/boards?=) that covers all repos in this project is maintained on this (the [docs](.)) repo. This board will be used when significant coordinated development is taking place. Otherwise not.

